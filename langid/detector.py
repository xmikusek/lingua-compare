#!/usr/bin/python3

import langid, sys

langid.set_languages(["cs", "en", "de", "la"])

for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    res = langid.classify(line)
    if res[0] == "cs":
        print("%s\tces" % (line))
    elif res[0] == "en":
        print("%s\teng" % (line))
    elif res[0] == "de":
        print("%s\tdeu" % (line))
    elif res[0] == "la":
        print("%s\tlat" % (line))
    else:
        print("%s\tunk" % (line))