#!/usr/bin/python3

import sys

nsize = int(sys.argv[1])

if nsize != 0:
    for line in sys.stdin:
        if line[-1] == "\n":
            line = line[:-1]
        line = line.split("\t")
        ngrams = line[0].split(" ")
        for i in range(0, len(ngrams) - nsize + 1):
            print("%s\t%s" % (" ".join(ngrams[i:i+nsize]), line[1]))
else:
    for line in sys.stdin:
        print(line, end="")
