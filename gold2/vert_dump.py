#!/usr/bin/python3

# Example usage:
#   decodevert europarl7_cs | vert2plain | ./vert_dump.py ces | ./clean_up.py | head -n 100000 > europarl7_cs.plain

import sys

glue = False
first = True

for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    if line.startswith("<"):
        if line.startswith("<s"):
            if not first:
                print("\t%s" % (sys.argv[1]))
            else:
                first = False
            glue = True
        if line.startswith("<g"):
            glue = True
    else:
        if glue:
            glue = False
        else:   
            print(" ", end="")
        print(line, end="")
        
