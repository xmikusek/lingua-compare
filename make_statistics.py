#!/usr/bin/python3

import sys

model_name_tlb = {
    0: "Gold",
    1: "Lingua",
    2: "pycld2",
    3: "fasttext_ftz",
    4: "fasttext_bin",
    5: "cld3",
    6: "langid",
}

languages = {
    "ces": 0,
    "eng": 1,
    "deu": 2,
    "lat": 3,
    "unk": 4,
}

input_count = int(sys.argv[1])

line_count = 0
result = {}
sentence = ""
loaded = 0
success = 0
failed = 0

statistics = {}
for i in range(input_count):
    conf_matrix = []
    for j in range(len(languages)):
        conf_matrix.append([0 for _ in range(len(languages))])
    statistics[i] = {"success": 0, "failed": 0, "error": {}, "conf_matrix": conf_matrix}

for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    line = line.split("\t")
    if len(line) != 2:
        raise Exception(line)
    if line_count % input_count == 0:
        sentence = line[0]
        result[0] = line[1]
    else:
        result[line_count % input_count] = line[1]

    line_count += 1
    if line_count >= input_count:
        loaded += 1
        line_count -= input_count
        rset = set()
        for key in result:
            rset.add(result[key])
        if len(rset) != 1:
            failed += 1
            print(sentence)
            print(result)
        else:
            success += 1
        for i in range(0, len(result)):
            if result[0] != result[i]:
                statistics[i]["failed"] += 1
                statistics[i]["error"]["%s->%s" % (result[0], result[i])] = statistics[i]["error"].get("%s->%s" % (result[0], result[i]), 0) + 1
                statistics[i]["conf_matrix"][languages[result[0]]][languages[result[i]]] += 1
            else:
                statistics[i]["success"] += 1
                statistics[i]["conf_matrix"][languages[result[0]]][languages[result[i]]] += 1
    

print("*** STATISTICS: ***")
print("Loaded: %d" % (loaded))
print("All success: %d - %.2f %%" % (success, success/loaded * 100))
print("All failed: %d - %.2f %%" % (failed, failed/loaded * 100))
for i in range(input_count):
    print("Method %s:" % (model_name_tlb[i]))
    print("\tsuccess: %d - %.2f %%" % (statistics[i]["success"], statistics[i]["success"]/loaded * 100))
    print("\tfailed: %d - %.2f %%" % (statistics[i]["failed"], statistics[i]["failed"]/loaded * 100))
    print("\terrors: ")
    print("\t\t", end="")
    print(statistics[i]["error"])
    print("\t\t", end="")
    print(statistics[i]["conf_matrix"])