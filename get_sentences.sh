#!/bin/bash

cat annotations/*hotovo.md | sed -e "s/\`[^\`]*\`{.error}//g" | grep -e "{lang=[^}]*}" | sed -e "s/{lang=\([^}]*\)}/{lang=\1}\n/g" | grep -e "{lang=[^}]*}" | sed -e "s/^[^\`]*\`//g" | sed -e "s/\`{lang=\([^}]*\)}[[:blank:]]*$/\t\1/g"
