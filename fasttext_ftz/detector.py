#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# source: https://fasttext.cc/docs/en/language-identification.html
# modified

DETECTION_VECTOR_SIZE = 20000000

import fasttext, sys

class LanguageIdentification:

    def __init__(self):
        pretrained_lang_model = "fasttext_ftz/lid.176.ftz"
        self.model = fasttext.load_model(pretrained_lang_model)

    def predict_lang(self, text):
        predictions = self.model.predict(text, k=DETECTION_VECTOR_SIZE)
        return predictions

LANGUAGE = LanguageIdentification()
for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    lang = LANGUAGE.predict_lang(line)[0]
    try:
        for i in range(DETECTION_VECTOR_SIZE):
            if lang[i] == "__label__cs":
                print("%s\tces" % (line))
                break
            elif lang[i] == "__label__en":
                print("%s\teng" % (line))
                break
            elif lang[i] == "__label__de":
                print("%s\tdeu" % (line))
                break
            elif lang[i] == "__label__la":
                print("%s\tlat" % (line))
                break
    except IndexError:
        print("%s\tunk" % (line))
