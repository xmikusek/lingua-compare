#!/bin/bash

./merger.py \
<(cat gold2/europarl7_cs.plain gold2/europarl7_en.plain gold2/europarl7_de.plain | ./ngram-gen.py $1) \
<(cat gold2/europarl7_cs.plain gold2/europarl7_en.plain gold2/europarl7_de.plain | ./ngram-gen.py $1 | sed -e "s/\t.*$//g" | ./lingua-build/target/release/lingua-compare) \
<(cat gold2/europarl7_cs.plain gold2/europarl7_en.plain gold2/europarl7_de.plain | ./ngram-gen.py $1 | sed -e "s/\t.*$//g" | ./pycld2/detector.py) \
<(cat gold2/europarl7_cs.plain gold2/europarl7_en.plain gold2/europarl7_de.plain | ./ngram-gen.py $1 | sed -e "s/\t.*$//g" | ./fasttext_ftz/detector.py) \
<(cat gold2/europarl7_cs.plain gold2/europarl7_en.plain gold2/europarl7_de.plain | ./ngram-gen.py $1 | sed -e "s/\t.*$//g" | ./fasttext_bin/detector.py) \
<(cat gold2/europarl7_cs.plain gold2/europarl7_en.plain gold2/europarl7_de.plain | ./ngram-gen.py $1 | sed -e "s/\t.*$//g" | ./cld3/detector.py) \
<(cat gold2/europarl7_cs.plain gold2/europarl7_en.plain gold2/europarl7_de.plain | ./ngram-gen.py $1 | sed -e "s/\t.*$//g" | ./langid/detector.py) \
| ./make_statistics.py 7