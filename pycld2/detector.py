#!/usr/bin/python3

import pycld2, sys

for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    res = pycld2.detect(line)
    if res[2][0][0] == "CZECH":
        print("%s\tces" % (line))
    elif res[2][0][0] == "ENGLISH":
        print("%s\teng" % (line))
    elif res[2][0][0] == "GERMAN":
        print("%s\tdeu" % (line))
    elif res[2][0][0] == "LATIN":
        print("%s\tlat" % (line))
    else:
        print("%s\tunk" % (line))